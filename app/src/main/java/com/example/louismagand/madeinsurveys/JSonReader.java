package com.example.louismagand.madeinsurveys;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONArray;
import org.json.JSONException;

public class JSonReader {

    private static String readBufferedReader(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public static JSONArray readJsonFromUrl() throws IOException, JSONException {
        URL madeInSurveysList = new URL("http://www.madeinsurveys.com/test/users.php");
        URLConnection connection = madeInSurveysList.openConnection();

        try {
            InputStream r = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(r));
            String jsonText = readBufferedReader(rd);
            return new JSONArray(jsonText);
        } catch(Exception e){
            System.out.println(e.toString());
            return null;
        }
    }
}