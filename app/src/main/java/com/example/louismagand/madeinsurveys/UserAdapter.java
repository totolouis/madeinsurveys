package com.example.louismagand.madeinsurveys;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Louis Magand on 01/07/2017.
 */
public class UserAdapter extends BaseAdapter{

    private List<UserModel> list;
    private Context context;
    private LayoutInflater inflater;

    public UserAdapter(List<UserModel> data, Context context) {
        this.list = data;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    //convert the XML file into a valid view
    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.row_user, parent, false);
        TextView tv_name = layout.findViewById(R.id.name);
        TextView tv_surname = layout.findViewById(R.id.surname);
        TextView tv_mail = layout.findViewById(R.id.mail);

        tv_name.setText(list.get(position).getName());
        tv_surname.setText(list.get(position).getSurname());
        tv_mail.setText(list.get(position).getMail());

        return layout;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get((i));
    }

    @Override
    public long getItemId(int i) {
        return i;
    }
}
