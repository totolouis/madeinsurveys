package com.example.louismagand.madeinsurveys;

/**
 * Created by Louis Magand on 01/07/2017.
 */
public class UserModel {

    String name;
    String surname;
    String mail;

    public UserModel(String name, String surname, String mail)
    {
        this.name = name;
        this.surname = surname;
        this.mail = mail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}
