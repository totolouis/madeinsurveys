package com.example.louismagand.madeinsurveys;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<UserModel> dataModels;
    private ListView list;
    JSONArray json_list_temp;
    private final JSonReader reader =  new JSonReader();

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button button = (Button) findViewById(R.id.btRefresh);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getDataJson();
                setDataOnList();
            }
        });

        list = (ListView) findViewById(R.id.listUser);

        dataModels= new ArrayList<>();

        getDataJson();
        setDataOnList();
    }

    public ArrayList<UserModel> translateJsonArrayToList(JSONArray json_list){
        ArrayList<UserModel> list = new ArrayList<>();
        try {
            for (int i = 0; i < json_list.length(); i++) {
                list.add(i, new UserModel(json_list.getJSONObject(i).getString("firstname"), json_list.getJSONObject(i).getString("lastname"),
                        json_list.getJSONObject(i).getString("email")));
            }
            return list;
        }catch (JSONException e) {
            System.out.println(e.toString());
            return null;
        }
    }

    public void getDataJson(){
        Thread thread = new Thread(new Runnable(){
            @Override
            public void run(){
                try {
                    json_list_temp = reader.readJsonFromUrl();
                }catch(Exception e){
                    System.out.println(e.toString());
                }
            }
        });
        thread.start();

        try {
            thread.join();
        }catch(InterruptedException e){
            System.out.println(e.toString());
        }
    }

    public void setDataOnList(){
        dataModels = translateJsonArrayToList(json_list_temp);
        UserAdapter a = new UserAdapter(dataModels, getApplicationContext());
        list.setAdapter(a);
    }
}
